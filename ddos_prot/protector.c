#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/netfilter_ipv4.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/inet.h>
//#include <sys/types.h>
//#include <sys/socket.h>
//#include<linux/netfilter.h>
#include <linux/udp.h>
#include <linux/icmp.h>
#include <linux/tcp.h>

MODULE_AUTHOR ("Stormdog");
MODULE_DESCRIPTION ("DDoS protection module");
MODULE_LICENSE ("GPL");

#define AttackedPort 0
#define MinePort 25465
#define SuspiciousSize 100
#define MaliciousSize 1500
#define MinePortRes 25565
#define MinePortRes2 25765
#define SshPort 10022

static struct nf_hook_ops nfin;

static unsigned int
hook_func_in (void *priv, struct sk_buff *skb,
	      const struct nf_hook_state *state)
{
  struct ethhdr *eth;
  struct iphdr *ip_header;
  struct tcphdr *tcp_header;	// tcp header struct
  struct udphdr *udp_header;	// udp header struct
  struct sk_buff *sock_buff;

  unsigned int sport = 0, dport = 0, size = 0;

  sock_buff = skb;

  if (!sock_buff)
    {
      printk (KERN_INFO "Not socket buffer\n");
      return NF_ACCEPT;
    }

  ip_header = (struct iphdr *) skb_network_header (sock_buff);
  if (!ip_header)
    {
      printk (KERN_INFO "Not IP packet\n");
      return NF_ACCEPT;
    }
//if UDP PACKET
  if (ip_header->protocol == IPPROTO_UDP)
    {
      //udp_header = (struct udphdr *)skb_transport_header(sock_buff); //может вызвать проблемы с распознаванием портов

      udp_header = (struct udphdr *) ((__u32 *) ip_header + ip_header->ihl);

      sport = htons ((unsigned short int) udp_header->source);	//sport now has the source port
      dport = htons ((unsigned short int) udp_header->dest);	//dport now has the dest port
      size = (unsigned short int) udp_header->len;
     // printk (KERN_INFO "UDP here\n");
    }
  else if (ip_header->protocol == IPPROTO_TCP) //Эту часть можно закомментировать,т.к. атаки идут только по udp
    {
      //tcp_header = (struct tcphdr *)skb_transport_header(sock_buff); //doing the cast this way gave me the same problem

      tcp_header = (struct tcphdr *) ((__u32 *) ip_header + ip_header->ihl);	//this fixed the problem

      sport = htons ((unsigned int) tcp_header->source);	//sport now has the source port
      dport = htons ((unsigned short int) tcp_header->dest);	//dport now has the dest port
      size = (unsigned short int) tcp_header->window;
    //  printk (KERN_INFO "TCP here\n");
    }
  eth = (struct ethhdr *) skb_mac_header (skb);
  ip_header = (struct iphdr *) skb_network_header (skb);
  //printk (KERN_INFO "src mac %pM, dst mac %pM\n", eth->h_source, eth->h_dest);
  //printk (KERN_INFO "src IP addr: %pI4\n", &ip_header->saddr);
  //printk (KERN_INFO "Dest port: %d, size: %d\n", dport, size);
  if ((dport == AttackedPort) && (size > SuspiciousSize))
    {
      printk (KERN_INFO "Probably attack, dgram dropped. Info: port %d, size: %d\n", dport, size);
      printk (KERN_INFO "src IP addr: %pI4\n", &ip_header->saddr);
      return NF_DROP;
    }
  if ((dport != MinePort) || (dport != MinePortRes) || (dport != MinePortRes2) || (dport != SshPort)) 
    {
	  if (size == MaliciousSize) {
	  printk (KERN_INFO "Probably attack, dgram dropped. Info: port %d, size: %d\n", dport, size);
      printk (KERN_INFO "src IP addr: %pI4\n", &ip_header->saddr);
      return NF_DROP;
  }
    }
  return NF_ACCEPT;
}

static int __init
init_main (void)
{
  nfin.hook = hook_func_in;
  nfin.hooknum = NF_INET_PRE_ROUTING;
  nfin.pf = PF_INET;
  nfin.priority = NF_IP_PRI_FIRST;
  nf_register_net_hook (&init_net, &nfin);

  return 0;
}



static void __exit
cleanup_main (void)
{
  nf_unregister_net_hook (&init_net, &nfin);

}

module_init (init_main);
module_exit (cleanup_main);
