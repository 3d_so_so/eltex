#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/sem.h>

union semun {
 int val;                  /* значение для SETVAL */
 struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
 unsigned short *array;    /* массивы для GETALL, SETALL */
                           /* часть, особенная для Linux: */
 struct seminfo *__buf;    /* буфер для IPC_INFO */
}arg;
int main(int argc, char *argv[])
{
    struct shmid_ds s_ize;
    struct sembuf for_s={0,1,0};
    struct sembuf for_c={1,-1,0};
    char c[30]={0}, *n;
   key_t k;
   int cshmid,j=0,sem=0;
   char mas[10];
   long hash=0;
 FILE* fd;
 k=ftok("/etc/fstab",10);
 sem=semget(k,2,0);
 cshmid=shmget(k,0,0);
   if (cshmid<0) {
    printf("Error: %s\n", strerror(errno));
    exit(-1);
   }
   shmctl(cshmid,IPC_STAT,&s_ize);
   printf("Size of memory %d\n",s_ize.shm_segsz);
   n=(char*)shmat(cshmid,0,0);
   strcpy(n,argv[1]);
   printf("File %s\n",argv[1]);
       semop(sem,&for_s,1);
   if(semop(sem,&for_c,1)>-1){
        strcpy(c,n);
    printf("Rezult %s\n",c);
   }
    semop(sem,&for_s,1);

    return 0;
}

