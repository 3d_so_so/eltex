#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <malloc.h>
#include <wait.h>
void main (int argc, char *argv[])
{
    int i=0;
    int *mas, stat;
    pid_t *pids;
    mas=(int*)calloc((argc-1),sizeof(int));
    pids=(pid_t*)malloc((argc-1)*sizeof(pid_t));
    for (i=1; i<argc; i++){
        printf("File %s\n",argv[i]);
        pids[i]=fork();
        if (pids[i]==0){
            if(execl("./main", "main", argv[i], NULL)<0){
                printf("Can't start process %d \n", pids[i]);
            }
        }

    }
    for (i=1; i<argc; i++){
        waitpid(pids[i],&stat, 0);
        mas[i]=stat;
        printf("Pid=%d status=%d for file %s \n", pids[i], WEXITSTATUS(stat), argv[i]);
        }

}
