#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#define C 100
long main(int argc, char *argv[])
{
    FILE *f1;
    int i=0;
    char buf[C]={0}, a[C]={0},c;
    long j=0;
    f1=fopen(argv[1], "r");
    if (f1==NULL){
        printf("File not found \n");
        exit(-1);
    }
    while(!feof(f1)){
        c=fgetc(f1);
        if (c==EOF) {
            printf("End of file \n");
        }
        i=i+1;
        a[i]=c;
        j=j+a[i];

    }
    sleep(5);
   printf("Process %d in file %s counted %ld \n",getpid(), argv[1], j);
   fclose(f1);
    return j;
}
