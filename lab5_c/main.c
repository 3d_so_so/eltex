#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>
void *library_handler;

extern int mult(int, int );
extern double divi(int, int );
int main()
{
    int (*mult)(int a, int b);
    double (*divi)(int a, int b);
    int i,j;
    printf("Enter numbers\n");
    scanf("%d%d",&i,&j);
    library_handler=dlopen("./libmd.so",RTLD_LAZY);
    if (!library_handler) {
        fprintf(stderr,"dlopen() error: %s\n", dlerror());exit(1);}
    //void *dlsym(void *handle, char *symbol);
    mult=dlsym(library_handler,"mult");
    divi=dlsym(library_handler, "divi");
    printf("Multiple = %d ", mult(i,j));
    printf("Divide = %f", divi(i,j));
    dlclose(library_handler);
    return 0;
}
