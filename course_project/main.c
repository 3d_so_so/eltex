#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <pthread.h>
#include <sys/resource.h>
#define C 1023
#define portt1 32000
#define portt2 32001
#define portu1 32002
#define portu2 32004
#define portu3 32006
#define portu4 32008
#define broadcst "192.168.1.255"
//ГЛОБАЛЬНЫЕ ПЕРЕМЕННЫЕ ДЛЯ ПОТОКОВ
int sock_tcp_1, sock_tcp_2, sock_udp_1, sock_udp_2, sock_udp_3, sock_udp_4;	//дескрипторы сокетов
int n = 5;			//число сообщений
char get[] = { "GET" };
char post[] = { "POST" };

struct sockaddr_in brcast_1, brcast_2, brcast_3, brcast_4, lst_1, lst_2;
struct message
{
  int t;
  int ns;
  char mes[C];
} *speech;			//структура сообщения
struct
{
  pthread_mutex_t mutex;
  int counter;
} cck =
{
PTHREAD_MUTEX_INITIALIZER};

void *
udpost ()
{
  pthread_detach (pthread_self ());
  int k = 0;
  // setpriority(PRIO_PROCESS,getpid(),10);
  pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, NULL);
  printf ("Started sending POSTs\n");
  while (1)			//рассылаем UDP POST, ожидая заполнения
    {
      pthread_mutex_lock (&cck.mutex);
      if (cck.counter != n)
	{
	  pthread_mutex_unlock (&cck.mutex);
	  sendto (sock_udp_1, post, sizeof (post), 0,
		  (struct sockaddr *) &brcast_1, sizeof (brcast_1));
	  sendto (sock_udp_3, post, sizeof (post), 0,
		  (struct sockaddr *) &brcast_3, sizeof (brcast_3));
	  printf ("Sending POST to 1-st type of clients\n");
	  k = rand () % 6;
	  printf ("%d \n", k);
	  sleep (k);
	}
      else
	{
	  pthread_mutex_unlock (&cck.mutex);
	  continue;
	}
    }

}

void *
udpget ()
{
  //pthread_detach(pthread_self());
  int k = 0;
  //setpriority(PRIO_PROCESS,getpid(),10);
  pthread_setcancelstate (PTHREAD_CANCEL_DISABLE, NULL);
  printf ("Started sending GETs\n");
  while (1)			//рассылаем UDP GET о наличии сообщений
    {
      pthread_mutex_lock (&cck.mutex);
      if (cck.counter != 0)
	{
	  pthread_mutex_unlock (&cck.mutex);
	  sendto (sock_udp_2, get, sizeof (get), 0,
		  (struct sockaddr *) &brcast_2, sizeof (brcast_2));
	  sendto (sock_udp_4, get, sizeof (get), 0,
		  (struct sockaddr *) &brcast_4, sizeof (brcast_4));
	  printf ("Sending GET to 2-nd type of clients\n");
	  k = rand () % 6;
	  printf ("%d \n", k);
	  sleep (k);
	}
      else
	{
	  pthread_mutex_unlock (&cck.mutex);
	  continue;
	}
    }

}

int firsthandler (int acceptor);
void *
tcpost (void *acceptor1)
{
  int abc = 0;
  int acc_1 = *((int *) acceptor1);
  int bytes_time = 0, bytes_length = 0, bytes_char = 0;
  int sizze = 0;
  sizze = sizeof (struct sockaddr_in);
  while (1)
    {
      pthread_mutex_lock (&cck.mutex);
      if (cck.counter < n)
	{
	  pthread_mutex_unlock (&cck.mutex);
	  printf ("Waiting for accept!!\n");
	  fflush (stdout);
	  acc_1 = accept (sock_tcp_1, (struct sockaddr *) &lst_1, &sizze);
	  if (acc_1 > 0)
	    {
	      abc = firsthandler (acc_1);
	      pthread_mutex_lock (&cck.mutex);
	      cck.counter++;
	      printf ("Counter %d\n", cck.counter);
	      pthread_mutex_unlock (&cck.mutex);
	      // printf("Just 2 make sure socket had enough time to close\n");
	      //sleep(20-speech[cck.counter-1].t);
	      //sleep(speech[cck.counter].t);
	      abc = 0;
	      // printf("TCPRECIEVER recieved time %d, lenght %d, message %s\n",speech[cck.counter-1].ns,speech[cck.counter-1].t,speech[cck.counter-1].mes);
	    }
	  else
	    continue;
	}
      else
	{
	  pthread_mutex_unlock (&cck.mutex);
	  printf ("Stack if full!!!\n");
	  sleep (speech[cck.counter].t);
	  continue;
	}
    }

}

int
firsthandler (int acceptor)
{
  int bytes_time = 0;
  int zero = 0;
  bytes_time =
    recv (acceptor, &speech[cck.counter], sizeof (struct message), 0);
  printf ("bytes recieved %d\n", bytes_time);
  printf ("message %s\n", speech[cck.counter].mes);
  printf ("time %d\n", speech[cck.counter].t);
  printf ("length %d\n", speech[cck.counter].ns);
  //send(acceptor,&zero,0,0);
  close (acceptor);
  return 0;
}

int
secondhadler (int acceptor_g)
{
  printf ("Cleaner connected!\n");
  printf ("Current time, length and message are %d  %d  %s\n",
	  speech[cck.counter].t, speech[cck.counter].ns,
	  speech[cck.counter].mes);
  send (acceptor_g, &speech[cck.counter - 1], sizeof (struct message), 0);
  speech[cck.counter - 1].t = 0;
  speech[cck.counter - 1].ns = 0;
  for (int j = 0; j < strlen (speech[cck.counter - 1].mes); j++)
    {
      speech[cck.counter - 1].mes[j] = 0;
    }
  printf ("Struct cleared length %d, time %d, message %s\n",
	  speech[cck.counter - 1].ns, speech[cck.counter - 1].t,
	  speech[cck.counter - 1].mes);
  close (acceptor_g);
}

void *
tcpget (void *acceptor2)
{
  int acc_2 = *(int *) acceptor2;
  pthread_detach (pthread_self ());
  int sizze = 0;
  sizze = sizeof (struct sockaddr_in);
  while (1)
    {
      // pthread_mutex_lock(&cck.mutex);
      if (cck.counter > 0)
	{
	  // pthread_mutex_unlock(&cck.mutex);
	  printf ("Waiting for accept!!\n");
	  acc_2 = accept (sock_tcp_2, (struct sockaddr *) &lst_2, &sizze);
	  if (acc_2 > 0)
	    {
	      secondhadler (acc_2);
	      //pthread_mutex_lock(&cck.mutex);
	      cck.counter--;
	      //pthread_mutex_unlock(&cck.mutex);
	      printf ("Counter %d\n", cck.counter);
	    }
	  else
	    continue;
	}
      else
	{
	  // pthread_mutex_unlock(&cck.mutex);
	  printf ("Stack is empty!!!\n");
	  sleep (rand () % 5);
	  continue;
	}
    }

}

int
main ()
{
  int acc_1 = 0, acc_2 = 0;
  pthread_t tid[4];
  pthread_attr_t dtsp;
  int dt;
  dt = pthread_attr_init (&dtsp);
  int broadcast = 1;
  int k = 0;
  int brcast = 1;
  //dt=pthread_attr_setdetachstate(&dtsp,PTHREAD_CREATE_DETACHED);
  lst_1.sin_family = AF_INET;
  lst_1.sin_addr.s_addr = INADDR_ANY;
  lst_1.sin_port = htons (portt1);
  lst_2.sin_family = AF_INET;
  lst_2.sin_addr.s_addr = INADDR_ANY;
  lst_2.sin_port = htons (portt2);
  brcast_1.sin_family = AF_INET;
  brcast_1.sin_addr.s_addr = inet_addr (broadcst);
  brcast_1.sin_port = htons (portu1);
  brcast_3.sin_family = AF_INET;
  brcast_3.sin_addr.s_addr = inet_addr (broadcst);
  brcast_3.sin_port = htons (portu3);
  brcast_2.sin_family = AF_INET;
  brcast_2.sin_addr.s_addr = inet_addr (broadcst);
  brcast_2.sin_port = htons (portu2);
  brcast_4.sin_family = AF_INET;
  brcast_4.sin_addr.s_addr = inet_addr (broadcst);
  brcast_4.sin_port = htons (portu4);
  speech = (struct message *) malloc (n * sizeof (struct message));	//выделяем память под "очередь сообщений"
  memset (speech, 0, sizeof (speech));
  sock_tcp_1 = socket (AF_INET, SOCK_STREAM, 0);	//создаем сокеты
  sock_tcp_2 = socket (AF_INET, SOCK_STREAM, 0);
  sock_udp_1 = socket (AF_INET, SOCK_DGRAM, 0);
  sock_udp_2 = socket (AF_INET, SOCK_DGRAM, 0);
  sock_udp_3 = socket (AF_INET, SOCK_DGRAM, 0);
  sock_udp_4 = socket (AF_INET, SOCK_DGRAM, 0);
  if (setsockopt
      (sock_udp_1, SOL_SOCKET, SO_BROADCAST, &broadcast,
       sizeof broadcast) == -1)
    {
      perror ("setsockopt (SO_BROADCAST) sockudp1");
      exit (1);
    }
  if (setsockopt
      (sock_udp_2, SOL_SOCKET, SO_BROADCAST, &broadcast,
       sizeof broadcast) == -1)
    {
      perror ("setsockopt (SO_BROADCAST) sockudp2");
      exit (1);
    }
  if (setsockopt
      (sock_udp_3, SOL_SOCKET, SO_BROADCAST, &broadcast,
       sizeof broadcast) == -1)
    {
      perror ("setsockopt (SO_BROADCAST) sockudp3");
      exit (1);
    }
  if (setsockopt
      (sock_udp_4, SOL_SOCKET, SO_BROADCAST, &broadcast,
       sizeof broadcast) == -1)
    {
      perror ("setsockopt (SO_BROADCAST) sockudp4");
      exit (1);
    }
  if ((sock_tcp_1 < 0) || (sock_tcp_2 < 0) || (sock_udp_1 < 0)
      || (sock_udp_2 < 0))
    {
      printf ("Error while opening socket\n");	//проверка на ошибку при открытии сокета
      exit (-1);
    }
  if (bind (sock_tcp_1, (struct sockaddr *) &lst_1, sizeof (lst_1)) < 0)
    {
      printf ("Error while binding 'Listen 1' with socket 1 tcp!!!\n");
      exit (-1);
    }
  if (bind (sock_tcp_2, (struct sockaddr *) &lst_2, sizeof (lst_2)) < 0)
    {
      printf ("Error while binding 'Listen 2' with socket 2 tcp!!!\n");
      exit (-1);
    }
  if (listen (sock_tcp_1, n) < 0)
    {
      printf ("Not listening tcp 1!!!\n");
      exit (-1);
    }
  if (listen (sock_tcp_2, n) < 0)
    {
      printf ("Not listening tcp 1!!!\n");
      exit (-1);
    }

  if ((pthread_create (&tid[0], NULL, udpost, NULL)) < 0)
    {
      printf ("Error while creating UDPPOST thread!\n");
      exit (-1);
    }
  if ((pthread_create (&tid[1], NULL, udpget, NULL)) < 0)
    {
      printf ("Error while creating UDPGET thread!\n");
      exit (-1);
    }
  if ((pthread_create (&tid[2], NULL, tcpost, (void *) &acc_1)) < 0)
    {
      printf ("Error while creating TCPPOST thread!\n");
      exit (-1);
    }
  if ((pthread_create (&tid[3], NULL, tcpget, (void *) &acc_2)) < 0)
    {
      printf ("Error while creating TCPPOST thread!\n");
      exit (-1);
    }
  for (;;)
    {
      sleep (0.001);
    }
  return 0;
}
