#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <unistd.h>
long result[10]={0};
int count=0, r;
pthread_mutex_t mtx=PTHREAD_MUTEX_INITIALIZER;
long *hashsum (char *name)
{ int *b;
    pthread_mutex_lock(&mtx);
    FILE *f1;
    int i=0;
    char c,a;
    long j=0;
    f1=fopen(name, "r");
    if (f1==NULL){
        printf("File not found \n");
        pthread_exit(-1);
    }
    while(!feof(f1)){
        c=fgetc(f1);
        if (c==EOF) {
            printf("End of file \n");
        }
        i=i+1;
        a=c;
        j=j+a;

    }
    result[count]=j;
    count++;
    r=rand()%5;
    sleep(r);
    b=count-1;
   printf("Func:Process %ld in file %s counted %ld \n",pthread_self(), name, j);
   fclose(f1);
   pthread_mutex_unlock(&mtx);
   return(b);


}
int main(int argc, char *argv[])
{   int *po;
int a;
    pthread_t mtxr[argc-1];
    for (int i=0; i<argc-1;i++){
        pthread_create(&mtxr[i],NULL,hashsum,argv[i+1]);
    printf("CREATED %ld\n", mtxr[i]);
    }
    for (int i=0; i<argc-1; i++){
            printf("WAITING for %ld thread\n", mtxr[i]);
            pthread_join(mtxr[i],&po);
            a=(int**)po;
            printf("Main:Hash sum of file %s is in %d with result %ld\n",argv[i+1],po,result[a]);
    }
    printf("Hello world!\n");


    return 0;
}
