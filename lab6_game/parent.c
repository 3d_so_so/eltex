#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <wait.h>
void main(int argc,char *argv[]){
int st[3], sy[3]; //coordinates of each player
int hp1, hp2, hp3, hp4; //health points
int i;
pid_t *gamers;
for (i=0; i<4; i++){ //getting primary coordinates
st[i]=rand(15);
sy[i]=rand(15);
}
gamers=(pid_t*)malloc(4*sizeof(pid_t));
while ((hp1!=0)&&(hp2!=0)&&(hp3!=0)||((hp2!=0)&&(hp3!=0)&&(hp4!=0))||((hp1!=0)&&(hp3!=0)&&(hp4!=0))||((hp1!=0)&&(hp2!=0)&&(hp4!=0))){
    for (i=1; i!=4; i++){
        gamers[i]=fork();
        if (gamers[i]==0){
            if(execl("./main","main",st[i],sy[i],argv[2], NULL)<0){
              printf("Gamer %d didnt join the game\n", gamers[i]);
            }
        }

    }
}

}
