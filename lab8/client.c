#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
struct message {
long type;
pid_t client;
char fn[10];
long hash;
};
int main(int argc, char *argv[])
{
    char c;
   key_t k;
   int msgid,n,j;
 struct message mes;
 FILE* fd;
 k=ftok("/etc/fstab",10);
 msgid=msgget(k,0);
   if (msgid<0) {
    printf("Error: %s\n", strerror(errno));
    exit(-1);
   }
   mes.type=1;
   mes.client=getpid();
   mes.hash=0;
   strcpy(mes.fn,argv[1]);
   printf("File %s\n",mes.fn);
       n=msgsnd(msgid,&mes,sizeof(mes), 0);
   if(n<0){
    printf("Bad message sent!\n");
    exit(-1);
   }
           n=msgrcv(msgid,&mes,sizeof(mes),mes.client,0);
        printf("Message %d received from server %ld\n",n, mes.hash);


    return 0;
}

